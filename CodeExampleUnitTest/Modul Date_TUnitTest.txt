defineModule('Troy.Date_TUnitTest', function () {

    requiredModule('Troy.Date');
    var unitTestFrameWork = requiredModule('Troy.TUnitTestFramework');
    var ut = new unitTestFrameWork();

	function Date_TUnitTest() {
	}

	Date_TUnitTest.prototype.asDdmmYyyy = function () {
		var date = new Date(1966,6,10);
		var resultIs = date.asDdmmYyyy('.');
		var resultShould = '10.07.1966';
		ut.AssertAreEqual(resultIs,resultShould);
	};
	
	//Example for failing UnitTest
	/*Date_TUnitTest.prototype.asReadableDate = function () {
		var date = new Date(1966,6,10);
		var resultIs = date.asReadableDate();
		var resultShould = '11.07.1966';
		ut.AssertAreEqual(resultIs,resultShould);
	};*/
	return Date_TUnitTest;	
});
﻿using System;
using System.IO;

namespace TextFileUtils
{
    public class Read
    {
        public static string ReadOldestFileContent(string sourcePath,string extension)
        {
            var lastFileName = string.Empty;

            foreach (var fileName in Directory.GetFiles(sourcePath, "*." + extension))
            {
                var lastDate = DateTime.MinValue;
                var currentFileDate = new FileInfo(fileName).CreationTime;

                if (currentFileDate <= lastDate) continue;

                lastDate = currentFileDate;
                lastFileName = fileName;
            }

            if (string.IsNullOrEmpty(lastFileName))
            {
                return string.Empty;
            }
            return File.ReadAllText(lastFileName);
        }
    }
}
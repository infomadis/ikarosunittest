﻿using System;
using Slack.Webhooks;

namespace SlackUtils
{
    public enum ChannelNames
    {
        IkarosUnitTests
    }

    public enum MessageType
    {
        Info,
        Warning,
        Error
    }

    public static class SlackMessageManager
    {
        /// <summary>
        /// npm Install-Package Slack.Webhooks
        /// </summary>
        /// <param name="strMessageText"></param>
        public static void SendMessage(string strMessageText,
                                       ChannelNames channelName,
                                       MessageType messageType,
                                       bool bIgnore = false
                                       )
        {
            const string URL = "{hier url zu Slack eintragen}";

            var slackClient = new SlackClient(URL);

            var bIconEmoji = SetEmoji(messageType, out var userNo);
            
            var slackMessage = new SlackMessage
            {
                Text = strMessageText,
                IconEmoji = bIconEmoji
            };

            SetChannelNameAndUserName(channelName, slackMessage, userNo);

            PostMessage(bIgnore, slackClient, slackMessage);
        }

        private static void PostMessage(bool bIgnore, SlackClient slackClient, SlackMessage slackMessage)
        {
            if (!bIgnore)
            {
                slackClient.Post(slackMessage);
            }
        }

        private static void SetChannelNameAndUserName(ChannelNames channelName, SlackMessage slackMessage, string userNo)
        {
            switch (channelName)
            {
                case ChannelNames.IkarosUnitTests:
                    // ReSharper disable once StringLiteralTypo
                    slackMessage.Channel = "#ikaros-unittests";
                    slackMessage.Username = "UnitTest-Bot" + userNo;
                    break;
                default:
                    throw new NotImplementedException();
            }
        }

        private static string SetEmoji(MessageType messageType, out string userNo)
        {
            string bIconEmojii;
            switch (messageType)
            {
                case MessageType.Error:
                    bIconEmojii = Emoji.NoEntry;
                    userNo = "1";
                    break;
                case MessageType.Info:
                    bIconEmojii = Emoji.FourLeafClover;
                    userNo = "2";
                    break;
                case MessageType.Warning:
                    bIconEmojii = Emoji.Warning;
                    userNo = "4";
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(messageType), messageType, null);
            }

            return bIconEmojii;
        }
    }
}
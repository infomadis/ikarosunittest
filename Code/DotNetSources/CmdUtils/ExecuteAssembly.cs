﻿using System.Diagnostics;
using System.IO;

namespace CmdUtils
{
    public static class ExecuteAssembly
    {
        public static bool Execute(string path, string file, string arguments)
        {
            bool result;

            //Attention! Please start the Program from 
            var process = new Process();
            using (process)
            {
                var startInfo = new ProcessStartInfo
                {
                    WorkingDirectory = path,
                    WindowStyle = ProcessWindowStyle.Hidden,
                    CreateNoWindow = true,
                    Arguments = arguments,
                    FileName = Path.Combine(path, file),
                    UseShellExecute = true,
                };

                process.StartInfo = startInfo;

                result = process.Start();

                process.WaitForExit();
            }

            return result;
        }
    }
}


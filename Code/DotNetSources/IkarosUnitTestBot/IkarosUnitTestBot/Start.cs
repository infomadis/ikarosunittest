﻿using SlackUtils;
using System;

namespace IkarosUnitTestBot
{  
    public class Start
    {
        public static void StartUnitTest()
        {
            Console.WriteLine("start UnitTest...");
            switch (IkarosUnitTestExecutor.Start())
            {
                case ExecutorResult.Success:
                    SlackMessageManager.SendMessage(DateTime.Now + " : " + "UnitTests completed without errors",
                        ChannelNames.IkarosUnitTests, MessageType.Info);
                    break;
                case ExecutorResult.Failure:
                    SlackMessageManager.SendMessage(DateTime.Now + " : " + "UnitTests completed with error",
                        ChannelNames.IkarosUnitTests, MessageType.Error);
                    break;
                case ExecutorResult.Nothing:
                    //nothing to do, so we shouldn't send any messages
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            Console.WriteLine("end UnitTests...");
        }
    }
}
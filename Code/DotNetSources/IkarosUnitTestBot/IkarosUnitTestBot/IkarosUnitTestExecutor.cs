﻿using System;
using System.Configuration;
using CmdUtils;
using SlackUtils;

namespace IkarosUnitTestBot
{
    public delegate string ExecuteIkarosUnitTest();

    public enum ExecutorResult
    {
        Success,
        Failure,
        Nothing
    }

    public class IkarosUnitTestExecutor
    {
        internal static ExecutorResult Start()
        {
            var executorResult = ExecutorResult.Nothing;

            //execute Ikaros-UnitTests
            Execute();

            //get last LogFile Content
            var logContent = TextFileUtils.Read.ReadOldestFileContent(ConfigurationManager.AppSettings["CustomAppLogPath"],
                                                                       "log");

            Console.WriteLine(logContent);

            //localize Error-Messages
            executorResult = AnalyzeLogFileAndSendMessage(logContent);

            return executorResult;
        }

        private static void Execute()
        {
            ExecuteAssembly.Execute(ConfigurationManager.AppSettings["IkarosAutomationPath"],
                "UnitTest.cmd",
                ConfigurationManager.AppSettings["IkarosAutomationArguments"]);
        }

        public static ExecutorResult AnalyzeLogFileAndSendMessage(string logContent, bool doNotSendMessage = false)
        {
            var executorResult = ExecutorResult.Nothing;
            const string macroFailureStart = "#EA#";
            const string macroFailureEnd = "#EE#";
            const string macroSuccessStart = "#SA#";
            const string macroSuccessEnd = "#SE#";
            var currentLogContent = string.Empty;

            //localize success-Messages
            currentLogContent = logContent;
            while (currentLogContent.IndexOf(macroSuccessStart,StringComparison.CurrentCultureIgnoreCase) > 0)
            {
                var successMessage = currentLogContent.Substring(currentLogContent.IndexOf(macroSuccessStart) + 4);
                successMessage = successMessage.Substring(0, successMessage.IndexOf(macroSuccessEnd));
                currentLogContent = currentLogContent.Substring(currentLogContent.IndexOf(macroSuccessEnd) + 4);

                SlackMessageManager.SendMessage(successMessage, ChannelNames.IkarosUnitTests,MessageType.Info, doNotSendMessage);
                executorResult = ExecutorResult.Success;
            }

            //localize error-Messages
            currentLogContent = logContent;
            while (currentLogContent.IndexOf(macroFailureStart, StringComparison.CurrentCultureIgnoreCase) > 0)
            {
                var errMessage = currentLogContent.Substring(currentLogContent.IndexOf(macroFailureStart) + 4);
                errMessage = errMessage.Substring(0, errMessage.IndexOf(macroFailureEnd));
                currentLogContent = currentLogContent.Substring(currentLogContent.IndexOf(macroFailureEnd) + 4);

                SlackMessageManager.SendMessage(errMessage, ChannelNames.IkarosUnitTests, MessageType.Error, doNotSendMessage);
                executorResult = ExecutorResult.Failure;
            }

            return executorResult;
        }


    }
}
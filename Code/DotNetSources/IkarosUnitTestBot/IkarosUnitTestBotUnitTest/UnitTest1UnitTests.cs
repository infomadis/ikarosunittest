
using IkarosUnitTestBot;
using NUnit.Framework;

namespace IkarosUnitTestBotUnitTests
{
    [TestFixture]
    public class UnitTest1UnitTests
    {
        [Test]
        public void TestMethod1()
        {
            var logContent = "Test";
            var result = IkarosUnitTestExecutor.AnalyzeLogFileAndSendMessage(logContent,true);
            Assert.AreEqual(ExecutorResult.Nothing, result);

            logContent = "Te#SA#s#SE#t";
            result = IkarosUnitTestExecutor.AnalyzeLogFileAndSendMessage(logContent, true);
            Assert.AreEqual(ExecutorResult.Success, result);

            logContent = "Te#EA#st#EE#";
            result = IkarosUnitTestExecutor.AnalyzeLogFileAndSendMessage(logContent,true);
            Assert.AreEqual( ExecutorResult.Failure, result);
        }
    }
}

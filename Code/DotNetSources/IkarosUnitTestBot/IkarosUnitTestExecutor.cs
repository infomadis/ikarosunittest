﻿using System;
using System.Configuration;

namespace IkarosUnitTestBot
{
    public delegate string ExecuteIkarosUnitTest();

    public class IkarosUnitTestExecutor
    {
        internal static bool Start()
        {
            bool result = false;

            //execute Ikaros-UnitTests
            Execute();

            //get last LogFile Content
            string logContent = TextFileUtils.ReadLastFileContent(ConfigurationManager.AppSettings["CustomAppLogPath"]);

            Console.WriteLine(logContent);

            //localize Error-Messages
            result = AnalyseLogFileAndSendMessage(logContent);

            return result;
        }

        private static void Execute()
        {
            ExecuteAssembly.Execute(ConfigurationManager.AppSettings["IkarosAutomationPath"]
                                  , ConfigurationManager.AppSettings["IkarosAutomationArguments"]);
        }

        public static bool AnalyseLogFileAndSendMessage(string logContent)
        {
            bool result = false;
            const string MAKROSTART = "#EA#";
            const string MAKROEND = "#EE#";

            //localize Error-Messages
            while (logContent.IndexOf(MAKROSTART) > 0)
            {
                string errMessage = logContent.Substring(logContent.IndexOf(MAKROSTART) + 4);
                errMessage = errMessage.Substring(0, errMessage.IndexOf(MAKROEND));
                logContent = logContent.Substring(logContent.IndexOf(MAKROEND) + 4);

                SlackMessageManager.SendMessage(errMessage, true);
                result = true;
            }
            return result;
        }


    }
}

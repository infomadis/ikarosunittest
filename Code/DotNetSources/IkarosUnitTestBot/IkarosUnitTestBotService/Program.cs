﻿using System.ServiceProcess;

namespace IkarosUnitTestBotService
{
    static class Program
    {
        /// <summary>
        /// Der Haupteinstiegspunkt für die Anwendung.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new Service1() 
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}

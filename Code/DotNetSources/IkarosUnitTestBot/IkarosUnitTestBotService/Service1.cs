﻿using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.InteropServices;
using System.ServiceProcess;
using System.Timers;
using IkarosUnitTestBot;

namespace IkarosUnitTestBotService
{
    public partial class Service1 : ServiceBase
    {
        [DllImport("advapi32.dll", SetLastError = true)]
        private static extern bool SetServiceStatus(System.IntPtr handle, ref ServiceStatus serviceStatus);


        public enum ServiceState
        {
            SERVICE_STOPPED = 0x00000001,
            SERVICE_START_PENDING = 0x00000002,
            SERVICE_STOP_PENDING = 0x00000003,
            SERVICE_RUNNING = 0x00000004,
            SERVICE_CONTINUE_PENDING = 0x00000005,
            SERVICE_PAUSE_PENDING = 0x00000006,
            SERVICE_PAUSED = 0x00000007,
        }
        [StructLayout(LayoutKind.Sequential)]
        public struct ServiceStatus
        {
            public int dwServiceType;
            public ServiceState dwCurrentState;
            public int dwControlsAccepted;
            public int dwWin32ExitCode;
            public int dwServiceSpecificExitCode;
            public int dwCheckPoint;
            public int dwWaitHint;
        };

        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            CreateEventLogIfNotExistingOrInitEventLog();
            
            // Update the service state to Start Pending.
            var serviceStatus = new ServiceStatus
            {
                dwCurrentState = ServiceState.SERVICE_START_PENDING,
                dwWaitHint = 100000
            };
            SetServiceStatus(this.ServiceHandle, ref serviceStatus);

            StartAllThreads();

            // Update the service state to Running.
            serviceStatus.dwCurrentState = ServiceState.SERVICE_RUNNING;
            SetServiceStatus(this.ServiceHandle, ref serviceStatus);

            eventLog1.WriteEntry("IkarosAutoImporter timer executed", EventLogEntryType.Information);
        }

        [ExcludeFromCodeCoverage]
        private void StartAllThreads()
        {
            var timer = new Timer { Interval = Convert.ToInt32(20) * 1000 };
            timer.Elapsed += (sender, args) => OnTimer(timer);
            timer.Start();
        }

        public void OnTimer(Timer timer)
        {
            timer.Stop();

            try
            {
                Start.StartUnitTest();
            }
            catch (Exception ex)
            {
                eventLog1.WriteEntry(ex.Message + ex.StackTrace, EventLogEntryType.Error);
            }
            finally
            {
                timer.Start();
            }
        }

        [ExcludeFromCodeCoverage]
        private void CreateEventLogIfNotExistingOrInitEventLog()
        {
            const string ServiceName = "IkarosUnitTestBotService2";
            const string LogName = "IkarosUnitTest";

            if (!EventLog.SourceExists(ServiceName))
            {
                EventLog.CreateEventSource(ServiceName, LogName);
            }
            
            eventLog1 = new EventLog();
            eventLog1.BeginInit();
            eventLog1.Source = ServiceName;
            eventLog1.Log = LogName;
            eventLog1.EndInit();
        }

        protected override void OnStop()
        {
        }

        private void eventLog1_EntryWritten(object sender, EntryWrittenEventArgs e)
        {

        }
    }
}

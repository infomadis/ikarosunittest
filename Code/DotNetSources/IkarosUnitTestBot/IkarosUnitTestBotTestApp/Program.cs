﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using IkarosUnitTestBot;

namespace IkarosUnitTestBotTestApp
{
    class Program
    {
        private static System.Timers.Timer timer;

        static void Main(string[] args)
        {
            var c = new Program();
            
            if (args.Length == 0)
            {
                c.SetTimer();
                Console.WriteLine("press Key to stop Ikaros UnitTests");
                Console.Read();
            }
            else
            {
                Console.WriteLine("start UnitTests once...");
                StartBot();
                Console.WriteLine("end UnitTests...");
            }
        }


        private static void StartBot()
        {
           Start.StartUnitTest();
        }


        private void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            timer.Stop();
            var c = new Program();
            StartBot();
            timer.Start();
        }


        private void SetTimer()
        {
            // Create a timer with a two second interval.
            timer = new System.Timers.Timer(2000);
            // Hook up the Elapsed event for the timer. 
            timer.Elapsed += OnTimedEvent;
            timer.AutoReset = true;
            timer.Enabled = true;
        }
    }
}

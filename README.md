# Ikaros Unit Test Slack Integration

## What is this program for ?

with this program code it is possible to write unit tests also with the Ikaros Basis or Plus version

## OnSave: automatic execution of UnitTests

Unit tests are executed automatically each time a module is changed or the unit test itself is changed. No manual steps are required. very effective when unknown developers make changes and ensure that no errors are "imported

## Message in "Slack"

Messages are displayed directly in Slack in any Slack channel immediately after an error occurs.
This provides an immediate warning in case of errors, especially when the last changes are transferred from
development system to the integration system, this can be a sensible solution.

## supported error correction

as soon as a change is made to the code, the unit test is executed within 20 seconds
and the results, whether successful or not, are displayed immediately

## generic call

it is not necessary to specify new unit tests, they are automatically identified and executed

## Contact person

For further questions please contact the developer:

Martin Schreck
email: martin.s@troy-bleiben.de




